! Converts phase-file to genotype file.
subroutine PhasoToGeno(phasefn, genofn, ncol, nrow)

  implicit none

  !! Arguments
  character(255), intent(in) :: phasefn, genofn
  integer, intent(in) :: ncol
  integer, intent(inout) :: nrow

  !! Local variables
  integer :: stat, i, animalid
  real, dimension(ncol) :: linea, lineb
  character(100) :: nChar, fmt

  write(nChar,*) ncol
  fmt='(i20,'//trim(adjustl(nChar))//'F5.2)'

  open(97, file=phasefn, status='OLD')
  open(98, file=genofn, status='UNKNOWN')
  i = 0
  do while (.TRUE.)
    read(97, *, iostat=stat) animalid, linea
    if (stat /= 0) exit
    read(97, *, iostat=stat) animalid, lineb
    write(98, fmt) animalid, linea + lineb
    i = i + 1
    if (i == nrow) exit
  end do

  close(97)
  close(98)

  nrow = i

end subroutine

subroutine PhasoToGenoInt(phasefn, genofn, ncol, nrow)

  implicit none

  !! Arguments
  character(255), intent(in) :: phasefn, genofn
  integer, intent(in) :: ncol
  integer, intent(inout) :: nrow

  !! Local variables
  integer :: stat, i, animalid
  integer, dimension(ncol) :: linea, lineb
  character(100) :: nChar, fmt

  write(nChar,*) ncol
  fmt='(i20,'//trim(adjustl(nChar))//'i2)'

  open(97, file=phasefn, status='OLD')
  open(98, file=genofn, status='UNKNOWN')
  i = 0
  do while (.TRUE.)
    read(97, *, iostat=stat) animalid, linea
    if (stat /= 0) exit
    read(97, *, iostat=stat) animalid, lineb
    write(98, fmt) animalid, linea + lineb
    i = i + 1
    if (i == nrow) exit
  end do

  close(97)
  close(98)

  nrow = i

end subroutine
