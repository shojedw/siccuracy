!! Merges two SNP chips

subroutine mergeChips(fnhd, fnld, fnout, hdcols, ldcols, outcols, nhd, hdid, nld, ldid, hdpos, ldpos,  missing, status)

  integer, intent(in) :: hdcols, ldcols, outcols, nhd, nld, missing
  character(300), intent(in) :: fnhd, fnld, fnout
  integer, intent(in), dimension(nhd) :: hdid
  integer, intent(in), dimension(nld) :: ldid
  integer, intent(in), dimension(hdcols) :: hdpos
  integer, intent(in), dimension(ldcols) :: ldpos
  integer, intent(out) :: status

  logical :: foundID
  integer :: stat, i, oldi, animalID
  real, dimension(outcols) :: output
  character(50)  :: fmt
  real, allocatable :: line(:)

  ! Open output file.
  open(71, file=fnout, status='UNKNOWN', iostat=stat)
  if (stat /= 0) then
    status = 1
    return
  end if

  write(fmt, '(i5)') outcols
  fmt='(i20,'//trim(adjustl(fmt))//'F5.2)'

  ! Read through HD file.
  allocate(line(hdcols))
  output(:) = missing
  i = 0
  oldi = 0
  open(72, file=fnhd, status='OLD')
  do while (.TRUE.)
    read(72, *, iostat=stat) animalID, line
    if (stat /= 0) exit

    !print *, animalID, line(1:5)

    ! Find animal in HD IDs.
    foundID = .FALSE.
    !oldi = i
    !if (oldi == 0) oldi=1
    do while (.TRUE.)
      if (i == nhd) i = 0
      i = i + 1
      !print *, i, oldi, hdid(i)
      if (hdid(i) == animalID) then
        foundID = .TRUE.
        exit
      end if
      if (i == oldi) exit
      if (i == nhd .and. oldi == 0) exit
    enddo
    oldi = i
    if (foundID) then
      output(hdpos) = line(1:hdcols)
      write(71, fmt) animalID, output
    end if

  end do
  close(72)
  deallocate(line)

  ! Read through LD file.
  allocate(line(ldcols))
  output(:) = missing
  i = 0
  oldi=0
  open(72, file=fnld, status='OLD')
  do while (.TRUE.)
    read(72, *, iostat=stat) animalID, line
    if (stat /= 0) exit


    ! Find animal in LD IDs.
    foundID = .FALSE.
    oldi = i
    if (oldi == 0) oldi=1
    do while (.TRUE.)
      if (i == nld) i = 0
      i = i + 1
      if (ldid(i) == animalID) then
        foundID = .TRUE.
        exit
      end if
      if (i == oldi) exit
      if (i == nld .and. oldi == 0) exit
    enddo
    oldi = i
    if (foundID) then
      output(ldpos) = line(1:ldcols)
      write(71, fmt) animalID, output
    end if

  end do
  close(72)
  deallocate(line)


  close(71)

end subroutine mergeChips


