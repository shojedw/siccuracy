

!! A subroutine that returns a matrix,
!! for testing how orientation works for R
subroutine test_matrix(m, n, res)

  integer, intent(in) :: m, n
  integer, dimension(n, m), intent(out) :: res
  integer :: i,j

  do i=1,n
    do j=1,m
      res(i,j) = i
    enddo
  enddo

end subroutine

subroutine col_meanvars(fn, n, Animals, nSNPs, means, vars)
  implicit none

  integer, parameter :: r8_kind = selected_real_kind(15,150)

  !! Arguments
  character(255), intent(in) :: fn
  integer, intent(in) :: nSNPs
  integer, intent(inout) :: n
  integer, dimension(n) :: Animals
  real(r8_kind), dimension(nSNPs), intent(out) :: means, vars

  !! Local variables
  integer :: animalID, i, stat
  real, dimension(nSNPs) :: genoin
  real(r8_kind), dimension(nSNPs) :: M, S, Mold, Sold

  M(:) = 0
  S(:) = 0
  Mold(:) = 0
  Sold(:) = 0

  open(10, file=fn, status='OLD')
  i = 0
  do
    read(10, *, iostat=stat) animalID, genoin
    if (stat /= 0)  exit
    if (n /= 0) then
      if ( ANY(Animals == animalID) .eqv. .FALSE.) cycle
    end if

    i = i + 1
    Mold(:) = M(:)
    Sold(:) = S(:)
    M = M + (genoin - Mold)/i
    S = S + (genoin - Mold) * (genoin - M)
  end do
  close(10)

  means = M
  vars = S/(i - 1)
  n = i

end subroutine

subroutine imp_acc4(truefn, imputefn, nSNPs, SNPgroups, nSNPgroups, nAnimals, &
    NAval, standardized, calcmeans, scalebysd, means, vars, rowcors, matcor, colcors)
  implicit none

  integer, parameter :: r8_kind = selected_real_kind(15, 150)

  !! Arguments
  character(255), intent(in) :: truefn, imputefn
  integer, intent(in) :: nSNPs, nSNPgroups, nAnimals, NAval, standardized, scalebysd, calcmeans
  integer, dimension(nSNPs), intent(in) :: SNPgroups
  real(r8_kind), dimension(nSnps), intent(inout) :: means, vars
  real(r8_kind), dimension(nSNPs), intent(out) :: colcors
  real(r8_kind), dimension(nAnimals, nSNPgroups), intent(out) :: rowcors
  real(r8_kind), intent(out), dimension(nSNPgroups) :: matcor

  !! Private variables
  integer :: stat, start, animalID, commonrows, i, j, k, l, maxanimal, minanimal, ianimalID
  real(r8_kind) :: t, t2, imp, imp2, tim, nan
  real(r8_kind), dimension(nSNPs) :: M, S, Mold, Sold
  real, dimension(nSNPs) :: genoin, imputed
  real, dimension(nAnimals, nSnps) :: trueMat
  !! For running correlation on columns
  integer, dimension(nAnimals) :: animalIndex
  real(r8_kind), dimension(nSNPs) :: cx, cy, cx2, cxy, cy2, ci
  !! For matrix
  real(r8_kind), dimension(nSNPgroups) :: mx, my, mx2, mxy, my2, mi
  !! For row
  real(r8_kind), dimension(nSNPgroups) :: rx, ry, rx2, rxy, ry2, ri

  nan = 0.0

  !! Read in true genotype fil
  open(10, file=truefn, status='OLD')
  i = 1
  do while (.TRUE.)
    read(10, *, iostat=stat) animalID, genoin
    if (stat /= 0) exit
    !print *, 'Read...', animalID
    animalIndex(i) = animalID
    trueMat(i, :) = genoin
    i = i + 1
  end do
  close(10)
  maxanimal = maxval(animalIndex)
  minanimal = minval(animalIndex)
  !print *, 'Min, max:', minanimal, maxanimal
  !print *, 'Index size', size(animalIndex)
  !print *, 'Index:', animalIndex

  !! Calculate scaling parameters (mean and var)
  if (calcmeans == 1) then
    if (standardized == 1) then
      M(:) = trueMat(1,:)
      S(:) = 0
      do i=2,nAnimals
        Mold(:) = M(:)
        Sold(:) = S(:)
        M = M + (trueMat(i, :) - Mold)/i
        S = S + (trueMat(i, :) - Mold) * (trueMat(i,:) - M)
      end do
      means = M
      vars = S/(nAnimals - 1)
      where (vars == 0.0) vars = 1.0
      if (scalebysd == 1) then
        vars = vars ** 2
      end if
    else
      means(:) = 0
      vars(:) = 1
    end if
  end if

  !! Sneak peak into imputed
  ianimalID = 0
  open(20, file=imputefn, status='OLD')
  do while (.TRUE.)
    read(20, *, iostat=stat) animalID
    if (stat /= 0) return
    if (animalID > minanimal) then  ! imputed animals has later animals than true.
      rewind(20)
      exit
    end if
    if (ianimalID > animalID) then  ! imputed animals are not ordered
      rewind(20)
      exit
    end if
    if (animalID == minanimal) then
      backspace(20)
      exit
    end if
    ianimalID = animalID
  end do
  !print *, 'Min, max, imp.a.ID', minanimal, maxanimal, animalID

  !! Go through imputed
  mx(:)=0; my(:)=0; mx2(:)=0; mxy(:)=0; my2(:)=0; mi(:)=0
  cx(:)=0; cy(:)=0; cx2(:)=0; cxy(:)=0; cy2(:)=0; ci(:)=0 !; cNA(:) = 0
  i = 1
  k = 1
  commonrows = 0
  do
    !i = i + 1
    read(20, *, iostat=stat) ianimalID, imputed
    if (stat /= 0) then
      exit
    endif
    !print *, 'Got', ianimalID, 'as imputed...'

    ! Find true genotype
    start = i
    k = 0
    do while (animalIndex(i) /= ianimalID)
      !print *, 'Found', animalIndex(i),'...'
      i = i + 1
      k = k + 1
      if (i > nAnimals) i = 1
      if (i == start) exit
      if (k > nAnimals + 3) then
        !print *, 'Oh oh.'
        exit
      end if
    end do
    if (animalIndex(i) /= ianimalID) then
      i = start
      cycle
    end if
    !print *, 'Settled on', animalIndex(i)

    commonrows = commonrows + 1
    !print *, 'True:', trueMat(i,:)
    !print *, 'Impu;', imputed(:)

    rx(:) = 0
    ry(:) = 0
    rx2(:) = 0
    rxy(:) = 0
    ry2(:) = 0
    !rNA = 0
    ri(:) = 0

    do j=1,nSnps
      if (imputed(j) == NAval .or. trueMat(i,j) == Naval) then
        !rNA = rNA + 1
        !cNA(j) = cNA(j) + 1
        cycle
      end if

      l = SNPgroups(j)

      ri(l) = ri(l) + 1
      mi(l) = ri(l) + 1
      ci(j) = ci(j) + 1

      t = (trueMat(i,j)-means(j))/vars(j)
      imp = (imputed(j)-means(j))/vars(j)
      t2 = t*t
      imp2 = imp*imp
      tim = t*imp

      cx(j) = cx(j) + t
      mx(l) = mx(l) + t
      rx(l) = rx(l) + t

      cy(j) = cy(j) + imp
      my(l) = my(l) + imp
      ry(l) = ry(l) + imp

      cx2(j) = cx2(j) + t2
      mx2(l) = mx2(l) + t2
      rx2(l) = rx2(l) + t2

      cy2(j) = cy2(j) + imp2
      my2(l) = my2(l) + imp2
      ry2(l) = ry2(l) + imp2

      cxy(j) = cxy(j) + tim
      mxy(l) = mxy(l) + tim
      rxy(l) = rxy(l) + tim

    end do ! end of j=1,nSNPs

    rowcors(i,:) = (ri * rxy - rx * ry) / sqrt( (ri * rx2 - rx**2) * (ri*ry2 - ry**2 ) )
    i = i + 1
  enddo
  close(10)
  close(20)

  matcor = (mi * mxy - mx * my) / sqrt( (mi * mx2 - mx**2) * (mi*my2 - my**2) )
  colcors = (ci * cxy - cx * cy) / sqrt( (ci * cx2 - cx**2) * (ci * cy2 - cy**2) )
  if (standardized == 1 .and. calcmeans == 0)  where (vars == 1) colcors = 1/nan

end subroutine
